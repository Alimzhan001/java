package com.secondyear.java.controllers;

import com.secondyear.java.model.Authorization;
import com.secondyear.java.services.AuthorizationServices;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AuthorizationController {

    private final AuthorizationServices authorizationServices;

    public AuthorizationController(AuthorizationServices authorizationServices) {
        this.authorizationServices = authorizationServices;
    }


    @GetMapping("/Authorizations")
    public ResponseEntity<?> getAuthorizations(){
        return ResponseEntity.ok ( authorizationServices.getAll ());
    }

    @GetMapping("/Authorization/{authorizationId}")
    public ResponseEntity<?> getAuthorization(@PathVariable Long authorizationId ){
        return ResponseEntity.ok ( authorizationServices.getById ( authorizationId ) );
    }

    @PostMapping("/Authorization")
    public ResponseEntity<?> saveAuthorization(@RequestBody Authorization authorization){
        return ResponseEntity.ok ( authorizationServices.create ( authorization ) );
    }

    @PutMapping("/Authorization")
    public ResponseEntity<?> updateAuthorization(@RequestBody Authorization authorization){
        return ResponseEntity.ok ( authorizationServices.update ( authorization ) );
    }

    @DeleteMapping("/Authorization/{authorizationId}")
    public void deleteAuthorization(@PathVariable Long authorizationId){
          authorizationServices.delete ( authorizationId );
    }

}

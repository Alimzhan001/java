package com.secondyear.java.controllers;

import com.secondyear.java.model.Catalog;
import com.secondyear.java.services.CatalogServices;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogController {

    private final CatalogServices catalogServices;

    public CatalogController(CatalogServices catalogServices) {
        this.catalogServices = catalogServices;
    }

    @GetMapping("/Catalogs")
    public ResponseEntity<?> getCatalogs(){
        return ResponseEntity.ok ( catalogServices.getAll () );
    }

    @GetMapping("/Catalog/{catalogId}")
    public ResponseEntity<?> getCatalog(@PathVariable Long catalogId){
        return ResponseEntity.ok ( catalogServices.getById ( catalogId ) );
    }

    @PostMapping("/Catalog")
    public ResponseEntity<?> saveCatalog(@PathVariable Catalog catalog){
        return  ResponseEntity.ok ( catalogServices.create ( catalog ) );
    }

    @PutMapping("/Catalog")
    public ResponseEntity<?> updateCatalog(@PathVariable Catalog catalog){
        return  ResponseEntity.ok ( catalogServices.update ( catalog ) );
    }

    @DeleteMapping("/Catalog/{catalogId}")
    public void delete(@PathVariable Long catalogId){
        catalogServices.delete ( catalogId );
    }

}

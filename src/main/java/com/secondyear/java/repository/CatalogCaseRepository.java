package com.secondyear.java.repository;

import com.secondyear.java.model.CatalogCase;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CatalogCaseRepository extends CrudRepository<CatalogCase, Long> {

    List<CatalogCase> findAll();
}

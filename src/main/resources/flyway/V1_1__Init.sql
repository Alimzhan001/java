Drop table if exists fond;
create table fond(
    id serial not null constraint fond_pk unique,
    fond_number varchar default 128,
    created_timestamp int8,
    created_by int8,
    updated_timestamp int8,
    updated_by int8
);
Drop table if exists company;
create table company(
    id serial not null constraint company_id unique,
    name_ru varchar default 128,
    name_kz varchar default 128,
    name_en varchar default 128,
    bin varchar default 32,
    parent_id int8 ,
    fond_id int8 constraint company_font__id_fk references fond (id),
    created_timestamp int8,
    created_by int8,
    updated_timestamp int8,
    updated_by int8
);
Drop table if exists company_unit;
create table company_unit(
    id serial not null  constraint company_unit_pk unique,
    name_ru varchar default 128,
    name_kz varchar default 128,
    name_en varchar default 128,
    parent_id int8,
    year int4,
    company_id int4 constraint company_unit_company_id_fk references company (id),
    index varchar default 16,
    created_timestamp int8,
    created_by int8,
    updated_timestamp int8,
    updated_by int8
);
DROP Table if exists auth;
create table auth (
    id serial not null constraint auth_id unique,
    login varchar unique default 255,
    email varchar default 255,
    password varchar default 128,
    role varchar default 255,
    forgot_password_key varchar default 255,
    forgot_password_key_timestamp int8,
    company_unit_id int8 constraint auth_company_unit_id_fk references company_unit (id)
);
Drop table if exists users;
create table users(
    id serial not null constraint user_pk unique,
    auth_id int8 constraint user_auth_id_fk references auth (id),
    name varchar default 128,
    fullname varchar default 128,
    surname varchar default 128,
    secondname varchar default 128,
    status varchar default 128,
    company_unit_id int8 constraint user_company_unit_id_fk references company_unit_id (id),
    password varchar default 128,
    last_login_timestamp int8,
    iin varchar default 32,
    is_active boolean,
    is_activated boolean,
    created_timestamp int8,
    created_by int8,
    updated_timestamp int8,
    updated_by int8
);
Drop table if exists record;
create table record(
    id serial not null constraint record_pk unique,
    number varchar default 128,
    type varchar default 128,
    company_unit_id int8 constraint record_company_unit_id_fk references company_unit_id (id),
    created_timestamp int8,
    created_by int8,
    updated_timestamp int8,
    updated_by int8
);
drop table if exists nomenclature_summary;
create table nomenclature_summary(
    id serial not null constraint nomenclature_summary_pk unique,
    number varchar default 128,
    year int4,
    company_unit_id int8 constraint nomenclature_summary_company_unit_id_fk references company_unit_id (id),
    created_timestamp int8,
    created_by int8,
    updated_timestamp int8,
    updated_by int8
);
Drop table if exists nomenclature;
create table nomenclature(
    id serial not null constraint nomenclature_pk unique,
    number varchar default 128,
    year int4,
    nomenclature_summary_id int8 constraint nomenclature_nomenclature_summary_id_fk references nomenclature_summary (id),
    company_unit_id int8 constraint nomenclature_company_unit_id_fk references company_unit_id (id),
    created_timestamp int8,
    created_by int8,
    updated_timestamp int8,
    updated_by int8
);
Drop table if exists case_index;
create table case_index(
    id serial not null constraint case_index_pk unique,
    case_index varchar default 128,
    title_ru varchar default 128,
    title_kz varchar default 128,
    title_en varchar default 128,
    storage_type int4,
    storage_year int4,
    note varchar default 128,
    company_unit_id int8 constraint case_index_company_unit_id_fk references company_unit_id (id),
    nomenclature_summary_id int8 constraint case_index_nomenclature_id_fk references nomenclature_summary (id),
    created_timestamp int8,
    created_by int8,
    updated_timestamp int8,
    updated_by int8
);
Drop table if exists act_destroy;
create table act_destroy(
    id serial not null constraint act_destroy_pk unique,
    number varchar default 128,
    reason varchar default 256,
    company_unit_id int8 constraint act_destroy_company_unit_id_fk references company_unit_id (id),
    created_timestamp int8,
    created_by int8,
    updated_timestamp int8,
    updated_by int8
);
drop table if exists location;
create table location(
    id serial not null constraint location_pk unique,
    row varchar default 64,
    line varchar default 64,
    column varchar default 64,
    box varchar default 64,
    company_unit_id int8 constraint location_company_unit_id_fk references company_unit_id (id),
    created_timestamp int8,
    created_by int8,
    updated_timestamp int8,
    updated_by int8
);
drop table if exists tempfiles;
create  table tempfiles(
    id serial not null constraint tempfiles_pk unique,
    file_binary text,
    file_binary_byte bytea
);
drop table if exists files;
create table files (
    id serial not null constraint file_pk unique,
    name varchar default 128,
    type varchar default 128,
    size int8,
    page_count int4,
    hash varchar default 128,
    is_deleted boolean,
    file_binary_id int8 constraint file_file_binary_id_fk references tempfiles (id),
    created_timestamp int8,
    created_by int8,
    updated_timestamp int8,
    updated_by int8
);